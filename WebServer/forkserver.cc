
/* My dumb server -- just an illustration */

#include <iostream>		// cout, cerr, etc
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>		// perror
#include <string.h>		// bcopy
#include <netinet/in.h>		// struct sockaddr_in
#include <unistd.h>		// read, write, etc
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <assert.h>
#include <time.h>
#include <sys/wait.h>

using namespace std;

const int BUFSIZE=10240;
const int FBUFSIZE = 5;
const int DATESIZE = 3;

int MakeServerSocket(const char *port) {
	const int MAXNAMELEN = 255;
	const int BACKLOG = 3;	
	char localhostname[MAXNAMELEN]; // local host name
	int s; 		
	int len;
	struct sockaddr_in sa; 
	struct hostent *hp;
	struct servent *sp;
	int portnum;	
	int ret;


	hp = gethostbyname("198.110.204.9");

	sscanf(port, "%d", &portnum);
	if (portnum ==  0) {
		sp=getservbyname(port, "tcp");
		portnum = ntohs(sp->s_port);
	}
	sa.sin_port = htons(portnum);

	bcopy((char *)hp->h_addr, (char *)&sa.sin_addr, hp->h_length);
	sa.sin_family = hp->h_addrtype;

	s = socket(hp->h_addrtype, SOCK_STREAM, 0);

	ret = bind(s, (struct sockaddr *)&sa, sizeof(sa));
	if (ret < 0)
		perror("Bad");
	listen(s, BACKLOG);
	cout << "Waiting for connection on port " << port << endl;
	return s;
}

string fileFilter(string input)
{
	return input.erase(input.find("/"),1);
}

void makeLog(int log, hostent * host, char * buf)
{
	string msg, rq;
	char * curTime = new char [BUFSIZE];
	time_t theTime;
	struct tm * timeinfo;

	msg += string(host -> h_name);
	msg += " - - ";

	time(&theTime);
	timeinfo = localtime(&theTime);
	strftime(curTime,BUFSIZE,"[%d/%b/%Y:%T %z] \"",timeinfo);
	msg += string(curTime);

	rq = string(buf);
	int cutoff = rq.find('\n');
	rq.erase(cutoff-1, rq.length()-1);
	msg += rq;
	
	msg += "\" 200 15225 \"-\" \"ia_archiver\"\n";

	int w = write(log, msg.c_str(), msg.length());
	assert(w == msg.length());
}

// Code stolen gracefully from https://stackoverflow.com/questions/16388510/evaluate-a-string-with-a-switch-in-c
constexpr unsigned int str2int(const char* str, int h = 0)
{
    return !str[h] ? 5381 : (str2int(str, h+1) * 33) ^ str[h];
}

char * makeMsg(string input, int file)
{
	char * 	msg; 
	off_t fsize;

	input.erase(0,input.find(".")+1);
	fsize = lseek(file, 0, SEEK_END);
	lseek(file, 0-fsize, SEEK_END);

	switch (str2int(input.c_str()))
	{
		case str2int("jpeg"):
		case str2int("jpg"):
			sprintf(msg,"HTTP/1.0 200 C'est Bien\nContent-Type: image/jpeg\nContent-Length: %li\n\n",fsize);
			break;
		case str2int("txt"):
			sprintf(msg,"HTTP/1.0 200 C'est Bien\nContent-Type: text/plain\nContent-Length: %li\n\n",fsize);
			break;
		case str2int("html"):
		case str2int("htm"):
			sprintf(msg,"HTTP/1.0 200 C'est Bien\nContent-Type: text/html\nContent-Length: %li\n\n",fsize);
			break;
		case str2int("gif"):
			sprintf(msg,"HTTP/1.0 200 C'est Bien\nContent-Type: image/gif\nContent-Length: %li\n\n",fsize);
			break;
		default:
			sprintf(msg,"HTTP/1.0 200 C'est Bien\nContent-Type: applicaiton/binary-data\nContent-Length: %li\n\n",fsize);
			break;
	}
	return msg;
}

void dirtyWork(int fd, sockaddr_in sa) {
	string filtered; // filter out unnecessary text
	char parsed[BUFSIZE]; // parse required info from request
	char * rq = new char [FBUFSIZE];
	int rlen, wlen;
	struct hostent* host;
	char buf[BUFSIZE];	// buffer in which to read
	int log = open("server.log", O_WRONLY);

	host = gethostbyaddr((const void *) &sa, sizeof(struct in_addr), AF_INET);

	// Read a bit of data
	int len = read(fd, buf, BUFSIZE);
	assert(len >= 0);

	makeLog(log,host,buf);
	buf[len] = 0;

	sscanf(buf,"%*s %s",parsed);
	filtered = fileFilter(string(parsed));

	// open file that was requested	
	int file = open(filtered.c_str(), O_RDONLY);
	if(file >= 0)
	{
		char * content;	
		char * msg = makeMsg(filtered,file);	
		cout << msg << endl;
		write(fd, msg, strlen(msg));

		do
		{
			rlen = read(file, rq, FBUFSIZE);
			assert(rlen >= 0);
			wlen = write(fd, rq, rlen); 
			assert(wlen == rlen);
		} while (rlen > 0);
	} else
	{
		// Write to the web client
		const char *msg = "HTTP/1.0 404 File not Found\nContent-Type: text/plain\n\n";
		cout << msg << endl;
		write(fd, msg, strlen(msg));
		const char * error = "You've managed to make a request that we cannot fulfil.\n\nGreat work, genius.\n\n";
		write(fd,error,strlen(error));
	}
	close(file);
	close(fd);
}

main(int argc, char *argv[]) {


	int s; 			// socket descriptor
	int len;		// length of reveived data
	int ret;		// return code from various system calls
	string line;


	s = MakeServerSocket(argv[1]);

	while (1) {
		struct sockaddr_in sa;
		int child, status;
		int sa_len = sizeof(sa);
		int fd = accept(s, (struct sockaddr *)&sa, (unsigned int *)&sa_len);
		assert(fd != -1);

		int pid = fork();

		if (pid == 0)
		{
			dirtyWork(fd,sa);
			exit(0);
		} else if (pid > 0)
		{
			close(fd);
			while (child != 0)
				child = waitpid(-1, &status, WNOHANG);
		}


	}  
	
}



