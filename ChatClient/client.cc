/* WARNING!  THIS CODE DOES NOT WORK FOR REASONABLE YET LARGE  FILE SIZES!!!! */

#include <iostream>		
#include <stdio.h>	
#include <string.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <string>
#include <cctype>




using namespace std;
const int BUFSIZE=102400;

void ErrorCheck(bool condition, const char *msg)
{
	if (condition)
	{
		perror(msg);
		exit(1);
	}
}

int MakeSocket(char *host, char *port) {
	int s; 			
	int len;	
	struct sockaddr_in sa; 
	struct hostent *hp;
	struct servent *sp;
	int portnum;	
	int ret;

	hp = gethostbyname(host);
	ErrorCheck(hp==0, "Gethostbyname");
	bcopy((char *)hp->h_addr, (char *)&sa.sin_addr, hp->h_length);
	sa.sin_family = hp->h_addrtype;
	sscanf(port, "%d", &portnum);
	if (portnum > 0) {
		sa.sin_port = htons(portnum);
	}
	else {
		sp=getservbyname(port, "tcp");
		portnum = sp->s_port;
		sa.sin_port = sp->s_port;
	}
	s = socket(hp->h_addrtype, SOCK_STREAM, 0);
	ErrorCheck(s == -1, "Could not make socket");
	ret = connect(s, (struct sockaddr *)&sa, sizeof(sa));
	ErrorCheck(ret == -1, "Could not connect");
	return s;
}

// compareChar and isQuit are ripped from:
	// https://thispointer.com/c-case-insensitive-string-comparison-using-stl-c11-boost-library/

bool compareChar(char & c1, char & c2)
{
	return (toupper(c1) == toupper(c2));
}

bool isQuit(string input)
{
	string quit = "QUIT";
	return ((input.size() == quit.size()) &&
		equal(input.begin(),input.end(), quit.begin(), &compareChar));
}

void closeClient(int sock, char * buf)
{

	int w = write(sock, "QUIT", 6);
	assert(w == 6);
	
	int r = read(sock,buf,BUFSIZE-1);
	assert(r>0);
	buf[r]=0;
	cout << buf << endl;

	close(sock);
}

int main(int argc, char *argv[]) {
	char buf[BUFSIZE];
	char input[BUFSIZE];
	string name = "NAME";
	string temp;
	int r,w;

	// Make a socket
	int sock = MakeSocket(argv[1], argv[2]);
	cout << "socket is " << sock << endl;
	assert(sock != -1);

	// NAME CODE

	cout << "Enter your name: ";
	cin >> temp;	
	name = name + temp;

	w = write(sock, name.c_str(),name.size()+2);
	assert(w == name.size()+2);

	r = read(sock, buf, BUFSIZE-1);
	assert(r > 0); // assert read is valid
	buf[r] = 0; // ensure EOL character is provided
	cout << buf << endl;

	
	cin.getline(input,BUFSIZE);
	do
	{
		// Code taken from in class notes until further comment
		w = write(sock, input, strlen(input)+2);
		assert(w == strlen(input)+2); //assert write is valid

		r = read(sock, buf, BUFSIZE-1);
		assert(r > 0); // assert read is valid
		buf[r] = 0; // ensure EOL character is provided
		cout << buf << endl;
		cin.getline(input,BUFSIZE);
	}
	while (!isQuit(string(input)));

	closeClient(sock,buf);

	return 0;
}



