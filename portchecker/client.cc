#include <iostream>		// cout, cerr, etc
#include <fstream>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>		// perror
#include <string.h>		// bcopy
#include <netinet/in.h>		// struct sockaddr_in
#include <unistd.h>		// read, write, etc
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <assert.h>
#include <time.h>
#include <sys/wait.h>

using namespace std;

const int maxportnum = 65535;
const int childmax = 5;

void ErrorCheck(bool condition, const char *msg)
{
	if (condition)
	{
		perror(msg);
		exit(1);
	}
}

int MakeSocket(const char *host, const int port) {
	int s;		
	int len;	
	struct sockaddr_in sa; 
	struct hostent *hp;
	struct servent *sp;
	int ret;

	hp = gethostbyname(host);
	ErrorCheck(hp==0, "Gethostbyname");
	bcopy((char *)hp->h_addr, (char *)&sa.sin_addr, hp->h_length);
	sa.sin_family = hp->h_addrtype;
	sa.sin_port = htons(port);
	s = socket(hp->h_addrtype, SOCK_STREAM, 0);
	ErrorCheck(s == -1, "Could not make socket");
	ret = connect(s, (struct sockaddr *)&sa, sizeof(sa));
	if (ret != -1)
		return s;
	else
	{
		close(s);
		return -1;
	}
}

void childWork(int child,string hostname) {
    int socket;

    // This math is based on evenly divisible integers of 65535.
    // Please repesect that and only use numbers that evenly divide 65535.
    // Otherwise, the code may break and that'll be sad.
    for (int port = 1 + (child * (maxportnum/childmax)); 
            port <= (maxportnum/childmax) + (maxportnum/childmax) * child; port++) {
        socket = MakeSocket(hostname.c_str(),port);
        if (socket != -1)
            printf("Port %i is openable\n", port);
        close(socket);
    }
}

int main(int argc, char *argv[]) {
    int socket,pid,status,child;
    string hostname;

    if (argc > 1)
        hostname = string(argv[1]);
    else
        hostname = "euclid.nmu.edu";

    for (child = 0; child < childmax; child++) {
        pid = fork(); 
        if (pid == 0) {
            childWork(child,hostname);
            exit(0);
        }
    }
    for (int i = 0; i < childmax; i++) 
        child = wait(&status);

    return 0;
}