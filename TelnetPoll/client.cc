#include <iostream>		
#include <stdio.h>	
#include <string.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <string>
#include <cctype>
#include <sys/poll.h>

using namespace std;
const int BUFSIZE=10240;

void ErrorCheck(bool condition, const char *msg)
{
	if (condition)
	{
		perror(msg);
		exit(1);
	}
}

int MakeSocket(char *host, char *port) {
	int s; 			
	int len;	
	struct sockaddr_in sa; 
	struct hostent *hp;
	struct servent *sp;
	int portnum;	
	int ret;

	hp = gethostbyname(host);
	ErrorCheck(hp==0, "Gethostbyname");
	bcopy((char *)hp->h_addr, (char *)&sa.sin_addr, hp->h_length);
	sa.sin_family = hp->h_addrtype;
	sscanf(port, "%d", &portnum);
	if (portnum > 0) {
		sa.sin_port = htons(portnum);
	}
	else {
		sp=getservbyname(port, "tcp");
		portnum = sp->s_port;
		sa.sin_port = sp->s_port;
	}
	s = socket(hp->h_addrtype, SOCK_STREAM, 0);
	ErrorCheck(s == -1, "Could not make socket");
	ret = connect(s, (struct sockaddr *)&sa, sizeof(sa));
	ErrorCheck(ret == -1, "Could not connect");
	return s;
}


int main(int argc, char *argv[]) {
	char buf[BUFSIZE];
	struct pollfd pollfds[2];
	int r,w;

	// Make a socket
	int sock = MakeSocket(argv[1], argv[2]);
	cout << "socket is " << sock << endl;
	assert(sock != -1);


	pollfds[0].fd = 0; // first is keyboard
	pollfds[0].events = POLLIN;
	pollfds[1].fd = sock; // second is server sock
	pollfds[1].events = POLLIN;

	while (1) {
		int result = poll(pollfds,2,-1);

		switch (result) {
			default:
				if (pollfds[0].revents & POLLIN) {
					// Keyboard
					r = read(0,buf,BUFSIZE);
					assert(r >= 0);
					w = write(sock,buf,strlen(buf)+2);
					assert(w == strlen(buf)+2);
				}
				if (pollfds[1].revents & POLLIN) {
					r = read(sock, buf, BUFSIZE-1);
					assert(r >= 0);
					buf[r] = 0;
					cout << buf << endl;
				}
		}
	}

	return 0;
}



