#include <iostream>		
#include <stdio.h>	
#include <string.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <string>
#include <cctype>
#include <arpa/inet.h>
#include <resolv.h>




using namespace std;
const int BUFSIZE=102400;

void ErrorCheck(bool condition, const char *msg)
{
	if (condition)
	{
		perror(msg);
		exit(1);
	}
}

int MakeSocket(char *host, char *port) {
	int s; 			
	int len;	
	struct sockaddr_in sa; 
	struct hostent *hp;
	struct servent *sp;
	int portnum;	
	int ret;

	hp = gethostbyname(host);
	ErrorCheck(hp==0, "Gethostbyname");
	bcopy((char *)hp->h_addr, (char *)&sa.sin_addr, hp->h_length);
	sa.sin_family = hp->h_addrtype;
	sscanf(port, "%d", &portnum);
	if (portnum > 0) {
		sa.sin_port = htons(portnum);
	}
	else {
		sp=getservbyname(port, "tcp");
		portnum = sp->s_port;
		sa.sin_port = sp->s_port;
	}
	s = socket(hp->h_addrtype, SOCK_STREAM, 0);
	ErrorCheck(s == -1, "Could not make socket");
	ret = connect(s, (struct sockaddr *)&sa, sizeof(sa));
	ErrorCheck(ret == -1, "Could not connect");
	return s;
}

/* Code found from this helpful StackOverflow article:
https://stackoverflow.com/questions/1688432/querying-mx-record-in-c-linux
Essentially, it queries a given domain name using nslookup-esque functions
Places valid mail exchanges in a 2D string array
And then returns the index of the most valuable MX server to be referenced
in the function that calls resolvmx(). */

int resolvmx(const char *name, char **mxs, int limit) {
    unsigned char response[NS_PACKETSZ];  /* big enough, right? */
    ns_msg handle;
    ns_rr rr;
    int mx_index, ns_index, len;
    char dispbuf[4096];

    if ((len = res_search(name, C_IN, T_MX, response, sizeof(response))) < 0) {
        /* WARN: res_search failed */
        return -1;
    }

    if (ns_initparse(response, len, &handle) < 0) {
        /* WARN: ns_initparse failed */
        return 0;
    }

    len = ns_msg_count(handle, ns_s_an);
    if (len < 0)
        return 0;

    for (mx_index = 0, ns_index = 0;
            mx_index < limit && ns_index < len;
            ns_index++) {
        if (ns_parserr(&handle, ns_s_an, ns_index, &rr)) {
            /* WARN: ns_parserr failed */
            continue;
        }
        ns_sprintrr (&handle, &rr, NULL, NULL, dispbuf, sizeof (dispbuf));
        if (ns_rr_class(rr) == ns_c_in && ns_rr_type(rr) == ns_t_mx) {
            char mxname[MAXDNAME];
            dn_expand(ns_msg_base(handle), ns_msg_base(handle) + ns_msg_size(handle), ns_rr_rdata(rr) + NS_INT16SZ, mxname, sizeof(mxname));
            mxs[mx_index++] = strdup(mxname);
        }
    }

    return mx_index;
}
void pingPong(string input, int sock)
{
	char buf[BUFSIZE];
	int w = write(sock,(input+ "\r\n").c_str(),input.size()+2);
	assert(w == input.size()+2);

	int r = read(sock, buf, BUFSIZE-1);
	assert(r>0);
	buf[r]=0;
	switch(buf[0])
	{
		case '1':
		case '2':
		case '3':
			break;
		default:
			cout << buf << endl;
			exit(1);
	}
}

string sendMessage()
{
	string temp,message;
	while(message != ".")
	{
		getline(cin,message);
		temp = temp + message + "\r\n";
	}
	return temp;
}

int main() {
	string from,temp,to;
	char  buf[BUFSIZE];
	char * mxs[10];
	int r,w,mxindex;

	sprintf(buf,"25"); // g++ complains if I statically assign

	// Figure out MX server (IF TIME)
	cout << "Enter the email domain you would like to send a message to: ";
	cin >> to;
	mxindex = resolvmx(to.c_str(),mxs,10);
	assert(mxindex > 0);

	// Make a socket to email server
	int sock = MakeSocket(mxs[mxindex-1],buf);
	assert(sock != -1);

	r = read(sock, buf, BUFSIZE-1);
	assert(r>0);
	cout << buf << endl;

	// Ask for servername
	cout << "Enter server name: ";
	cin >> temp;
	pingPong("HELO " + temp, sock);
	from = temp;
	
	// Ask for username
	cout << "Enter username: ";
	cin >> temp;
	from = temp + '@' + from;
	pingPong("MAIL FROM:<" + from + ">",sock);

	// Ask for recipient
	cout << "Enter recipient name: ";
	cin >> temp;
	to = temp + "@" + to;
	pingPong("RCPT TO:<" + to + ">",sock);

	// Ask for data
	cout << "Enter your message, and end it with . on a line by itself.\n";
	pingPong("DATA",sock);
	sprintf(buf, 
			"Date: Mon, 3 Feb 2020 12:00\r\nFrom: %s\r\nSubject: Test Email\r\n%s",
			from.c_str(),sendMessage().c_str());
	pingPong(string(buf),sock);

	// Quit
	pingPong("QUIT",sock);
	close(sock);

	cout << "Message sent. Thank you :)\n";


	return 0;
}



