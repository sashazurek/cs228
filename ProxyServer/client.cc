
/* My dumb server -- just an illustration */

#include <iostream>		// cout, cerr, etc
#include <algorithm>
#include <fstream>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>		// perror
#include <string.h>		// bcopy
#include <netinet/in.h>		// struct sockaddr_in
#include <unistd.h>		// read, write, etc
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <assert.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/poll.h>

using namespace std;

const int BUFSIZE=10240;
const int FBUFSIZE = 5;
const int DATESIZE = 3;

void ErrorCheck(bool condition, const char *msg)
{
	if (condition)
	{
		perror(msg);
		exit(1);
	}
}

int MakeServerSocket(const char *port) {
	const int MAXNAMELEN = 255;
	const int BACKLOG = 3;	
	char localhostname[MAXNAMELEN]; // local host name
	int s; 		
	int len;
	struct sockaddr_in sa; 
	struct hostent *hp;
	struct servent *sp;
	int portnum;	
	int ret;


	hp = gethostbyname("198.110.204.9");

	sscanf(port, "%d", &portnum);
	if (portnum ==  0) {
		sp=getservbyname(port, "tcp");
		portnum = ntohs(sp->s_port);
	}
	sa.sin_port = htons(portnum);

	bcopy((char *)hp->h_addr, (char *)&sa.sin_addr, hp->h_length);
	sa.sin_family = hp->h_addrtype;

	s = socket(hp->h_addrtype, SOCK_STREAM, 0);

	ret = bind(s, (struct sockaddr *)&sa, sizeof(sa));
	if (ret < 0)
		perror("Bad");
	listen(s, BACKLOG);
	cout << "Waiting for connection on port " << port << endl;
	return s;
}

int MakeSocket(const char *host,const char *port) {
	int s; 			
	int len;	
	struct sockaddr_in sa; 
	struct hostent *hp;
	struct servent *sp;
	int portnum;	
	int ret;

	hp = gethostbyname(host);
	ErrorCheck(hp==0, "Gethostbyname");
	bcopy((char *)hp->h_addr, (char *)&sa.sin_addr, hp->h_length);
	sa.sin_family = hp->h_addrtype;
	sscanf(port, "%d", &portnum);
	if (portnum > 0) {
		sa.sin_port = htons(portnum);
	}
	else {
		sp=getservbyname(port, "tcp");
		portnum = sp->s_port;
		sa.sin_port = sp->s_port;
	}
	s = socket(hp->h_addrtype, SOCK_STREAM, 0);
	ErrorCheck(s == -1, "Could not make socket");
	ret = connect(s, (struct sockaddr *)&sa, sizeof(sa));
	ErrorCheck(ret == -1, "Could not connect");
	return s;
}

bool isText(char * buf) {
	string rq = string(buf);

	if (rq.find("Content-Type: text/html") == string::npos) {
		return false;
	}
	else return true;
}

// code taken gracefully from https://stackoverflow.com/a/14678964 
void ReplaceStringInPlace(std::string& subject, const std::string& search,
                          const std::string& replace) {
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos) {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
}

string fileFilter(string input)
{
	if (input.find("Easter.html") == string::npos)
		return "no";
	else
		return "Easter.html";
}

void dirtyWork(int client) {
	char buf[BUFSIZE];	// buffer in which to read
	char parsed[BUFSIZE];
	struct pollfd pollfds[2];
	int r,w;
	bool flag = false;
	string temp;
	bool text = false;

	pollfds[0].fd = client; // first is client
	pollfds[0].events = POLLIN;
	pollfds[1].fd = MakeSocket("euclid.nmu.edu","80");
	pollfds[1].events = POLLIN;

	do {
		int result = poll(pollfds,2,-1);
		switch (result) {
			default:
				if (pollfds[0].revents & POLLIN) {
					// client
						// read from browser
						// send to euclid
					r = read(pollfds[0].fd,buf,BUFSIZE);

					sscanf(buf,"%*s %s",parsed);
					string filtered = fileFilter(string(parsed));
					if (filtered != "Easter.html") {
						if (r > 0) {
							w = write(pollfds[1].fd,buf,strlen(buf)+2);
							assert(w == strlen(buf)+2);
						} 
						else {
							flag = true;
						}
					} else {
						const char *msg = "HTTP/1.0 451 Unavailable For Legal Reasons\nContent-Type: text/plain\n\n";
						write(pollfds[0].fd, msg, strlen(msg));
						const char * error = "This transgression against the state will be noted.\n\n";
						write(pollfds[0].fd,error,strlen(error));
						flag = true;
					}
				}
				if (pollfds[1].revents & POLLIN) {
					// server
						// read from server
						// send to client
						do
						{
							r = read(pollfds[1].fd, buf, BUFSIZE);
							assert(r >= 0);
							text = isText(buf);
							if (text) {
								temp = string(buf);
								ReplaceStringInPlace(temp,"course","videos");
								ReplaceStringInPlace(temp,"Course","Videos");
								ReplaceStringInPlace(temp,"class","video");
								ReplaceStringInPlace(temp,"Class","Video");
								w = write(pollfds[0].fd, temp.c_str(), r); 
								assert(w == r);
							} else {
							w = write(pollfds[0].fd, buf, r); 
							assert(w == r);
							}
						} while (r > 0);
						flag = true;
					}
		}
	} while (flag != true);

	close(client);
}

main(int argc, char *argv[]) {


	int s; 			// socket descriptor
	int len;		// length of reveived data
	int ret;		// return code from various system calls
	string line;


	s = MakeServerSocket(argv[1]);

	while (1) {
		struct sockaddr_in sa;
		int child,status;
		int sa_len = sizeof(sa);
		int client = accept(s, (struct sockaddr *)&sa, (unsigned int *)&sa_len);
		assert(client != -1);

		dirtyWork(client);
	}  
}
